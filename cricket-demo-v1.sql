-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: cricket_demo_v1
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `cricket_demo_v1`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `cricket_demo_v1` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cricket_demo_v1`;

--
-- Table structure for table `BaseModal`
--

DROP TABLE IF EXISTS `BaseModal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BaseModal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BaseModal`
--

LOCK TABLES `BaseModal` WRITE;
/*!40000 ALTER TABLE `BaseModal` DISABLE KEYS */;
INSERT INTO `BaseModal` VALUES (1,'2019-02-28 20:01:49.194757','2019-02-28 20:01:49.194800',1,0),(2,'2019-02-28 20:02:36.924783','2019-02-28 20:02:36.924857',1,0),(3,'2019-02-28 20:03:29.539889','2019-02-28 20:03:29.539952',1,0),(4,'2019-02-28 20:04:02.196865','2019-02-28 20:04:02.196921',1,0),(5,'2019-02-28 20:07:38.781474','2019-02-28 20:07:38.781515',1,0),(6,'2019-02-28 20:08:05.745090','2019-02-28 20:08:05.745133',1,0),(7,'2019-02-28 20:10:07.202658','2019-02-28 20:10:07.202704',1,0),(8,'2019-02-28 20:10:46.881325','2019-02-28 20:10:46.881369',1,0),(9,'2019-02-28 20:11:31.187908','2019-02-28 20:11:31.187982',1,0),(10,'2019-02-28 20:12:10.981662','2019-02-28 20:12:10.981718',1,0),(11,'2019-02-28 20:12:52.300663','2019-02-28 20:12:52.300739',1,0),(12,'2019-02-28 20:13:52.016118','2019-02-28 20:13:52.016164',1,0),(13,'2019-02-28 20:14:41.049525','2019-02-28 20:14:41.049578',1,0),(14,'2019-02-28 20:15:23.861159','2019-02-28 20:15:23.861202',1,0),(15,'2019-02-28 20:16:10.819028','2019-02-28 20:16:10.819080',1,0),(16,'2019-02-28 20:17:09.874136','2019-02-28 20:17:09.874217',1,0),(17,'2019-02-28 20:17:49.083155','2019-02-28 20:17:49.083201',1,0),(18,'2019-02-28 20:21:06.915201','2019-02-28 20:21:06.915271',1,0),(19,'2019-02-28 20:22:00.662846','2019-02-28 20:22:00.662890',1,0),(20,'2019-02-28 20:22:49.820749','2019-02-28 20:22:49.820803',1,0),(21,'2019-02-28 20:27:05.246362','2019-02-28 20:27:05.246411',1,0),(22,'2019-02-28 20:28:06.167176','2019-02-28 20:28:06.167232',1,0),(23,'2019-02-28 20:29:06.462350','2019-02-28 20:29:06.462405',1,0),(24,'2019-02-28 20:30:01.164244','2019-02-28 20:30:01.164291',1,0),(25,'2019-02-28 20:30:43.592866','2019-02-28 20:30:43.592921',1,0),(26,'2019-02-28 20:31:51.234152','2019-02-28 20:31:51.234196',1,0),(27,'2019-02-28 20:32:41.332932','2019-02-28 20:32:41.332979',1,0),(28,'2019-02-28 20:33:25.985943','2019-02-28 20:33:25.985997',1,0),(29,'2019-02-28 20:35:39.975154','2019-02-28 20:35:39.975231',1,0),(30,'2019-02-28 20:36:29.460287','2019-02-28 20:36:29.460343',1,0),(31,'2019-02-28 20:37:12.417120','2019-02-28 20:37:12.417166',1,0),(32,'2019-02-28 20:38:09.299675','2019-02-28 20:38:09.299733',1,0),(33,'2019-02-28 20:38:57.447165','2019-02-28 20:38:57.447205',1,0);
/*!40000 ALTER TABLE `BaseModal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'admin');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add user',6,'add_user'),(17,'Can change user',6,'change_user'),(18,'Can delete user',6,'delete_user'),(19,'Can add base modal',7,'add_basemodal'),(20,'Can change base modal',7,'change_basemodal'),(21,'Can delete base modal',7,'delete_basemodal'),(22,'Can add country',8,'add_country'),(23,'Can change country',8,'change_country'),(24,'Can delete country',8,'delete_country'),(25,'Can add match',9,'add_match'),(26,'Can change match',9,'change_match'),(27,'Can delete match',9,'delete_match'),(28,'Can add team',10,'add_team'),(29,'Can change team',10,'change_team'),(30,'Can delete team',10,'delete_team'),(31,'Can add match player details',11,'add_matchplayerdetails'),(32,'Can change match player details',11,'change_matchplayerdetails'),(33,'Can delete match player details',11,'delete_matchplayerdetails'),(34,'Can add score card',12,'add_scorecard'),(35,'Can change score card',12,'change_scorecard'),(36,'Can delete score card',12,'delete_scorecard'),(37,'Can add score card player',13,'add_scorecardplayer'),(38,'Can change score card player',13,'change_scorecardplayer'),(39,'Can delete score card player',13,'delete_scorecardplayer');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'IN','India'),(2,'AUS','Australia'),(3,'SA','South Africa'),(4,'NZ','New Zeeland'),(5,'BD','Bangladesh'),(6,'SL','Sri Lanka');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-02-28 18:54:12.768369','1','admin',1,'[{\"added\": {}}]',3,1),(2,'2019-02-28 18:55:15.550505','1','admin@admin.com',2,'[{\"changed\": {\"fields\": [\"image_uri\", \"user_type\", \"jersy_number\", \"player_type\"]}}]',6,1),(3,'2019-02-28 18:56:20.553404','1','admin@admin.com',2,'[{\"changed\": {\"fields\": [\"groups\"]}}]',6,1),(4,'2019-02-28 18:57:29.753782','1','India',1,'[{\"added\": {}}]',8,1),(5,'2019-02-28 18:57:42.556006','2','Australia',1,'[{\"added\": {}}]',8,1),(6,'2019-02-28 18:57:57.019258','3','South Africa',1,'[{\"added\": {}}]',8,1),(7,'2019-02-28 18:58:21.706687','4','New Zeeland',1,'[{\"added\": {}}]',8,1),(8,'2019-02-28 18:58:46.098547','5','Bangladesh',1,'[{\"added\": {}}]',8,1),(9,'2019-02-28 18:59:07.324903','6','Sri Lanka',1,'[{\"added\": {}}]',8,1),(10,'2019-02-28 19:00:05.785499','1','India',1,'[{\"added\": {}}]',10,1),(11,'2019-02-28 19:01:37.408528','2','Australia',1,'[{\"added\": {}}]',10,1),(12,'2019-02-28 19:02:10.760011','3','South Africa',1,'[{\"added\": {}}]',10,1),(13,'2019-02-28 19:02:35.434571','4','Sri Lanka',1,'[{\"added\": {}}]',10,1),(14,'2019-02-28 19:03:17.695851','5','New Zeeland',1,'[{\"added\": {}}]',10,1),(15,'2019-02-28 19:43:21.920793','18','mickel@dispostable.com',2,'[{\"changed\": {\"fields\": [\"last_name\", \"image_uri\"]}}]',6,1),(16,'2019-02-28 19:48:03.506817','6','Bangladesh',1,'[{\"added\": {}}]',10,1),(17,'2019-02-28 19:56:02.023517','1','First leauge-IND-AUS ODI series',2,'[{\"changed\": {\"fields\": [\"match_date\", \"match_result\"]}}]',9,1),(18,'2019-02-28 19:58:29.910740','2','Second leauge-IND-AUS ODI series',2,'[{\"changed\": {\"fields\": [\"match_date\", \"match_result\"]}}]',9,1),(19,'2019-02-28 19:58:42.639073','2','Second leauge-IND-BAN ODI series',2,'[{\"changed\": {\"fields\": [\"name\"]}}]',9,1),(20,'2019-02-28 20:10:07.204641','7','Saurabh',1,'[{\"added\": {}}]',13,1),(21,'2019-02-28 20:10:46.995676','8','Virat',1,'[{\"added\": {}}]',13,1),(22,'2019-02-28 20:11:31.191104','9','Raina',1,'[{\"added\": {}}]',13,1),(23,'2019-02-28 20:12:10.983547','10','Dhawan',1,'[{\"added\": {}}]',13,1),(24,'2019-02-28 20:12:52.399348','11','Dhoni',1,'[{\"added\": {}}]',13,1),(25,'2019-02-28 20:13:52.020021','12','Saurabh',1,'[{\"added\": {}}]',13,1),(26,'2019-02-28 20:14:41.052785','13','Virat',1,'[{\"added\": {}}]',13,1),(27,'2019-02-28 20:15:23.960551','14','Raina',1,'[{\"added\": {}}]',13,1),(28,'2019-02-28 20:16:10.821123','15','Dhoni',1,'[{\"added\": {}}]',13,1),(29,'2019-02-28 20:17:09.876953','16','Ponting',1,'[{\"added\": {}}]',13,1),(30,'2019-02-28 20:17:49.194494','17','David',1,'[{\"added\": {}}]',13,1),(31,'2019-02-28 20:21:06.918274','18','Rishard',1,'[{\"added\": {}}]',13,1),(32,'2019-02-28 20:22:00.664537','19','Peter',1,'[{\"added\": {}}]',13,1),(33,'2019-02-28 20:22:49.822962','20','Andrew',1,'[{\"added\": {}}]',13,1),(34,'2019-02-28 20:27:05.391676','21','tamim',1,'[{\"added\": {}}]',13,1),(35,'2019-02-28 20:28:06.169700','22','Jahangir',1,'[{\"added\": {}}]',13,1),(36,'2019-02-28 20:29:06.464740','23','Rafigul',1,'[{\"added\": {}}]',13,1),(37,'2019-02-28 20:30:01.166381','24','Azhar',1,'[{\"added\": {}}]',13,1),(38,'2019-02-28 20:30:43.696731','25','Ponting',1,'[{\"added\": {}}]',13,1),(39,'2019-02-28 20:31:51.236029','26','David',1,'[{\"added\": {}}]',13,1),(40,'2019-02-28 20:32:41.335012','27','Rishard',1,'[{\"added\": {}}]',13,1),(41,'2019-02-28 20:33:25.987887','28','Andrew',1,'[{\"added\": {}}]',13,1),(42,'2019-02-28 20:35:40.096584','29','K Rodes',1,'[{\"added\": {}}]',13,1),(43,'2019-02-28 20:36:29.464715','30','Mickel',1,'[{\"added\": {}}]',13,1),(44,'2019-02-28 20:37:12.419178','31','Alpha',1,'[{\"added\": {}}]',13,1),(45,'2019-02-28 20:38:09.302076','32','Brain',1,'[{\"added\": {}}]',13,1),(46,'2019-02-28 20:38:57.449587','33','Redmond',1,'[{\"added\": {}}]',13,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'contenttypes','contenttype'),(7,'match','basemodal'),(8,'match','country'),(9,'match','match'),(11,'match','matchplayerdetails'),(12,'match','scorecard'),(13,'match','scorecardplayer'),(10,'match','team'),(6,'match','user'),(5,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-02-28 18:52:25.031886'),(2,'contenttypes','0002_remove_content_type_name','2019-02-28 18:52:26.486743'),(3,'auth','0001_initial','2019-02-28 18:52:32.110229'),(4,'auth','0002_alter_permission_name_max_length','2019-02-28 18:52:32.393837'),(5,'auth','0003_alter_user_email_max_length','2019-02-28 18:52:32.467825'),(6,'auth','0004_alter_user_username_opts','2019-02-28 18:52:32.539164'),(7,'auth','0005_alter_user_last_login_null','2019-02-28 18:52:32.610385'),(8,'auth','0006_require_contenttypes_0002','2019-02-28 18:52:32.667207'),(9,'auth','0007_alter_validators_add_error_messages','2019-02-28 18:52:32.744772'),(10,'auth','0008_alter_user_username_max_length','2019-02-28 18:52:32.813114'),(11,'auth','0009_alter_user_last_name_max_length','2019-02-28 18:52:32.883914'),(12,'match','0001_initial','2019-02-28 18:53:03.439824'),(13,'admin','0001_initial','2019-02-28 18:53:06.195622'),(14,'admin','0002_logentry_remove_auto_add','2019-02-28 18:53:06.290251'),(15,'sessions','0001_initial','2019-02-28 18:53:07.313200');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('773o8y5dt7ecwk6d6h63jzddlzujou0m','MmVmMGNlMDdjMGIxNTgwNTNiYmY5NjE3MmM3YmVhNTgyZjVhMjMxZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NWVlODA2NTQ2YTNhZTM0MDk1YjVlZjliYWE5YWQ5ZjNmOTJhYjNkIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2019-03-14 19:16:13.830696'),('x5wwgeiqjvnj7bvr1wj29wjfrfxs8sh9','MmVmMGNlMDdjMGIxNTgwNTNiYmY5NjE3MmM3YmVhNTgyZjVhMjMxZjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0NWVlODA2NTQ2YTNhZTM0MDk1YjVlZjliYWE5YWQ5ZjNmOTJhYjNkIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2019-03-14 20:08:06.143833');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match`
--

DROP TABLE IF EXISTS `match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `match_date` datetime(6) DEFAULT NULL,
  `match_result` varchar(100) DEFAULT NULL,
  `team1_id` int(11) NOT NULL,
  `team2_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `match_team1_id_1702348e_fk_team_id` (`team1_id`),
  KEY `match_team2_id_549268ae_fk_team_id` (`team2_id`),
  CONSTRAINT `match_team1_id_1702348e_fk_team_id` FOREIGN KEY (`team1_id`) REFERENCES `team` (`id`),
  CONSTRAINT `match_team2_id_549268ae_fk_team_id` FOREIGN KEY (`team2_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match`
--

LOCK TABLES `match` WRITE;
/*!40000 ALTER TABLE `match` DISABLE KEYS */;
INSERT INTO `match` VALUES (1,'First leauge-IND-AUS ODI series','2019-03-02 19:55:49.000000','IND won by 7 wicket',1,2),(2,'Second leauge-IND-BAN ODI series','2019-03-01 19:58:15.000000','BAN won by 2 runs',1,6),(3,'Third leauge AUS-NZ ODI series',NULL,NULL,2,5);
/*!40000 ALTER TABLE `match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_player_detail`
--

DROP TABLE IF EXISTS `match_player_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_player_detail` (
  `basemodal_ptr_id` int(11) NOT NULL,
  `match_id_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `team_id_id` int(11) NOT NULL,
  PRIMARY KEY (`basemodal_ptr_id`),
  KEY `match_player_detail_match_id_id_bf23b01e_fk_match_id` (`match_id_id`),
  KEY `match_player_detail_player_id_f8330012_fk_user_id` (`player_id`),
  KEY `match_player_detail_team_id_id_e3f8f6b6_fk_team_id` (`team_id_id`),
  CONSTRAINT `match_player_detail_basemodal_ptr_id_4d1012a6_fk_BaseModal_id` FOREIGN KEY (`basemodal_ptr_id`) REFERENCES `BaseModal` (`id`),
  CONSTRAINT `match_player_detail_match_id_id_bf23b01e_fk_match_id` FOREIGN KEY (`match_id_id`) REFERENCES `match` (`id`),
  CONSTRAINT `match_player_detail_player_id_f8330012_fk_user_id` FOREIGN KEY (`player_id`) REFERENCES `user` (`id`),
  CONSTRAINT `match_player_detail_team_id_id_e3f8f6b6_fk_team_id` FOREIGN KEY (`team_id_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_player_detail`
--

LOCK TABLES `match_player_detail` WRITE;
/*!40000 ALTER TABLE `match_player_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `match_player_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `score_card`
--

DROP TABLE IF EXISTS `score_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score_card` (
  `basemodal_ptr_id` int(11) NOT NULL,
  `runs` int(11) NOT NULL,
  `wicket` int(11) NOT NULL,
  `over` double NOT NULL,
  `status` varchar(10) NOT NULL,
  `point` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`basemodal_ptr_id`),
  KEY `score_card_match_id_ec31bc82_fk_match_id` (`match_id`),
  KEY `score_card_team_id_8e90029d_fk_team_id` (`team_id`),
  CONSTRAINT `score_card_basemodal_ptr_id_ce09546a_fk_BaseModal_id` FOREIGN KEY (`basemodal_ptr_id`) REFERENCES `BaseModal` (`id`),
  CONSTRAINT `score_card_match_id_ec31bc82_fk_match_id` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`),
  CONSTRAINT `score_card_team_id_8e90029d_fk_team_id` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `score_card`
--

LOCK TABLES `score_card` WRITE;
/*!40000 ALTER TABLE `score_card` DISABLE KEYS */;
INSERT INTO `score_card` VALUES (1,323,8,50,'won',3,1,1),(2,320,10,45.4,'loss',0,1,2),(3,245,5,48.2,'draw',1,2,1),(4,245,7,50,'draw',1,2,6),(5,305,10,50,'loss',0,3,2),(6,311,6,48.3,'won',3,3,5);
/*!40000 ALTER TABLE `score_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `score_card_player`
--

DROP TABLE IF EXISTS `score_card_player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score_card_player` (
  `basemodal_ptr_id` int(11) NOT NULL,
  `runs` int(11) NOT NULL,
  `wicket` int(11) NOT NULL,
  `over` double NOT NULL,
  `fifty` int(11) NOT NULL,
  `hundred` int(11) NOT NULL,
  `meadan_over` double NOT NULL,
  `fours` int(11) NOT NULL,
  `sixs` int(11) NOT NULL,
  `man_of_match` tinyint(1) NOT NULL,
  `status` varchar(10) NOT NULL,
  `player_id_id` int(11) NOT NULL,
  `score_id` int(11) NOT NULL,
  PRIMARY KEY (`basemodal_ptr_id`),
  KEY `score_card_player_player_id_id_5560a1c2_fk_user_id` (`player_id_id`),
  KEY `score_card_player_score_id_454a2e7c_fk_score_car` (`score_id`),
  CONSTRAINT `score_card_player_basemodal_ptr_id_75df4bba_fk_BaseModal_id` FOREIGN KEY (`basemodal_ptr_id`) REFERENCES `BaseModal` (`id`),
  CONSTRAINT `score_card_player_player_id_id_5560a1c2_fk_user_id` FOREIGN KEY (`player_id_id`) REFERENCES `user` (`id`),
  CONSTRAINT `score_card_player_score_id_454a2e7c_fk_score_car` FOREIGN KEY (`score_id`) REFERENCES `score_card` (`basemodal_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `score_card_player`
--

LOCK TABLES `score_card_player` WRITE;
/*!40000 ALTER TABLE `score_card_player` DISABLE KEYS */;
INSERT INTO `score_card_player` VALUES (7,60,1,1,1,0,0,8,2,0,'out',2,1),(8,120,1,0,0,1,0,12,3,0,'out',3,1),(9,30,4,7,0,0,1,1,1,0,'out',4,1),(10,90,3,5,1,0,1,3,2,0,'out',5,1),(11,120,1,0,0,1,0,12,3,0,'out',6,1),(12,101,1,2,0,1,0,10,3,0,'out',2,3),(13,132,2,5,0,1,0,12,5,0,'notout',3,3),(14,86,2,4,1,0,0,5,1,0,'out',4,3),(15,79,1,2,1,0,0,6,2,0,'notout',6,3),(16,80,1,2,1,0,0,5,1,0,'out',7,2),(17,102,0,0,0,1,0,6,2,0,'notout',8,2),(18,65,2,8,1,0,1,4,1,0,'out',9,2),(19,96,3,8,1,0,2,4,1,1,'notout',10,2),(20,145,1,2,0,1,0,15,3,0,'notout',11,2),(21,140,3,5,1,1,1,5,2,0,'notout',23,4),(22,91,2,6,1,0,0,5,2,0,'notout',24,4),(23,102,6,6,1,1,1,8,2,0,'out',25,4),(24,125,5,4,1,1,0,10,3,0,'out',26,4),(25,104,0,0,1,1,0,12,5,0,'out',7,5),(26,89,1,5,1,0,0,9,4,0,'notout',8,5),(27,84,5,7,1,1,1,6,4,1,'notout',9,5),(28,106,2,5,1,1,1,8,4,0,'notout',11,5),(29,150,1,1,2,1,0,15,6,1,'out',17,6),(30,120,6,4,2,1,1,12,4,0,'out',18,6),(31,200,0,0,4,2,0,25,10,0,'out',20,6),(32,155,1,0,3,1,0,15,5,0,'out',21,6),(33,111,2,5,2,1,0,12,6,0,'out',22,6);
/*!40000 ALTER TABLE `score_card_player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `logo_uri` varchar(100) NOT NULL,
  `club_state` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'India','team_logo/india_h2kcf1K.jpeg','Indian Cricket team'),(2,'Australia','team_logo/austrilia_pC1V2cV.png','Team Austri'),(3,'South Africa','team_logo/southA.png','South Flag'),(4,'Sri Lanka','team_logo/sriL.png','Sri Lanka Fan Club'),(5,'New Zeeland','team_logo/westI.jpeg','New Zee Young club'),(6,'Bangladesh','team_logo/cri-logo1.jpg','Bangla Worriers');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `email` varchar(254) NOT NULL,
  `image_uri` varchar(100) NOT NULL,
  `user_type` varchar(15) NOT NULL,
  `jersy_number` varchar(5) NOT NULL,
  `player_type` varchar(20) NOT NULL,
  `random_string` varchar(200) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `user_country_id_9852a6e8_fk_country_id` (`country_id`),
  KEY `user_team_id_9c9a1513_fk_team_id` (`team_id`),
  CONSTRAINT `user_country_id_9852a6e8_fk_country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  CONSTRAINT `user_team_id_9c9a1513_fk_team_id` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'pbkdf2_sha256$100000$xUjj00801SsB$ySaAH3+xgHSzgRgDhZCPEl+9rlbPWhLWASuJlnKaFEM=','2019-02-28 19:16:13.770143',1,'admin','','',1,1,'2019-02-28 18:53:40.000000','admin@admin.com','image_user/aus_7GHu87Z.jpeg','admin','1','batsman',NULL,NULL,NULL),(2,'pbkdf2_sha256$100000$mkjRjZytg7ac$F+vvjGsgm4QGzx8UNFN2tM+GVcgAlRdRcYqIHZeug0U=',NULL,0,'saurabh@dispostable.com','Saurabh','Player one',0,1,'2019-02-28 19:12:59.116481','saurabh@dispostable.com','','player','1','all rounder','saurdbef',1,1),(3,'pbkdf2_sha256$100000$ophqyPqfH2LT$0+fgviTkKPSCtrgGij+2r3ArSRWpaEYAIC+wSYU4D6A=',NULL,0,'virat@dispostable.com','Virat','Player two',0,1,'2019-02-28 19:19:10.685941','virat@dispostable.com','','player','2','all rounder','vira7e42',1,1),(4,'pbkdf2_sha256$100000$ZUwUHuTKxvZI$Y36nBH2n7TfnAENOeCzBPR1YtB/ltG1iOJkcWfD7TEU=',NULL,0,'raina@dispostable.com','Raina','Player three',0,1,'2019-02-28 19:25:58.119836','raina@dispostable.com','','player','2','batsman','rainpg0t',1,1),(5,'pbkdf2_sha256$100000$kCITsBTg875j$ULl8IE+lrUVojLJoACB8riMN2eOIJpb2ppzR//ICN8w=',NULL,0,'dhawan@dispostable.com','Dhawan','Player four',0,1,'2019-02-28 19:26:49.396913','dhawan@dispostable.com','','player','4','bowler','dhawzv26',1,1),(6,'pbkdf2_sha256$100000$XlqMbrYDHyXI$reQNsW53S7oMxKCk9e2xjgCJo0s0NLjADfimLE9ulVM=',NULL,0,'dhoni@dispostable.com','Dhoni','Player five',0,1,'2019-02-28 19:27:52.475005','dhoni@dispostable.com','','player','6','captain','dhontiai',1,1),(7,'pbkdf2_sha256$100000$2wKFUCTQmCPe$zbi5IJ04LQzgsqcVO4oSqhFvADJ9HbNNpAdx0ULnvJg=',NULL,0,'ponting@dispostable.com','Ponting','Player one',0,1,'2019-02-28 19:28:45.877081','ponting@dispostable.com','','player','2','batsman','pontf35l',2,2),(8,'pbkdf2_sha256$100000$vAMjsHrnkU6Y$nHDQlpDh51dIiRij/UoBBxzZh6H5ub+xHiGwpGdyV7I=',NULL,0,'david@dispostable.com','David','Player two',0,1,'2019-02-28 19:29:32.550954','david@dispostable.com','','player','7','captain','daviwg7i',2,2),(9,'pbkdf2_sha256$100000$2L1jStAqJJrU$kbeozNjw01GpklcuOFwKCqihH1/9PPkVnThF3Ap7rKM=',NULL,0,'richard@dispostable.com','Rishard','Player three',0,1,'2019-02-28 19:30:34.588914','richard@dispostable.com','','player','10','wicket keeper','richxn9f',2,2),(10,'pbkdf2_sha256$100000$HeWNlw2vedrs$gd6KESpc1THGjV7tMlYESVnPwS5gzL4IX7nCb1ZITpc=',NULL,0,'peter@dispostable.com','Peter','Player four',0,1,'2019-02-28 19:31:46.870281','peter@dispostable.com','','player','11','bowler','peteki0v',2,2),(11,'pbkdf2_sha256$100000$MwPES5MCrQ2Z$SZnr6wF1QCEjEz6dZNGjiXBTQz3o74AHjiOKkb1yzWI=',NULL,0,'andrew@dispostable.com','Andrew','Player four',0,1,'2019-02-28 19:32:32.537452','andrew@dispostable.com','','player','12','All rounder','andr01cy',2,2),(12,'pbkdf2_sha256$100000$Mz4Zfe82bBn7$7RnJ5kySTSVArpLi2KcYc42DSp4g+0uyBsz16udP+ek=',NULL,0,'peterson@dispostable.com','Peterson','Player one',0,1,'2019-02-28 19:34:22.075445','peterson@dispostable.com','','player','14','All rounder','peteuxdk',3,3),(13,'pbkdf2_sha256$100000$SCOflxVBKv6Y$HzX5ITZknfxiPPBgFuS/xJXYalLwDtvi7HjcYDLSwA4=',NULL,0,'john@dispostable.com','John','Player two',0,1,'2019-02-28 19:35:15.435296','john@dispostable.com','','player','15','captain','john58m2',3,3),(14,'pbkdf2_sha256$100000$p5hvfcA3mFUb$aumMHNs+qyeHIbctuqtWobHE2c6jTyi9ptLwG0SIQ3s=',NULL,0,'rockey@dispostable.com','Rocky','Player three',0,1,'2019-02-28 19:36:52.981520','rockey@dispostable.com','','player','2','batsman','rockucre',3,3),(15,'pbkdf2_sha256$100000$kdghk4vpdulG$EOlMp6cHZV75/+m4NW5SeYDbCiooKLoAXFHkCCVikCA=',NULL,0,'jonty@dispostable.com','Jonty','Player four',0,1,'2019-02-28 19:37:42.003677','jonty@dispostable.com','','player','15','bowler','jontamn1',3,3),(16,'pbkdf2_sha256$100000$G1pWCuZf01zJ$wldHZZI7xnH7Z5Fc0Hyx4EIdpNOcb6EqD0eilL4MUBo=',NULL,0,'gharam@dispostable.com','Gharam','Player five',0,1,'2019-02-28 19:38:26.064783','gharam@dispostable.com','','player','20','batsman','gharwxsf',3,3),(17,'pbkdf2_sha256$100000$rG07QCrkoEAF$pDtb9c+TZ4Vy0usrDQyzX3awe8eNRhNXjHW3lda6lFI=',NULL,0,'rodes@dispostable.com','K Rodes','Player one',0,1,'2019-02-28 19:39:19.160852','rodes@dispostable.com','','player','12','batsman','rode69ej',4,5),(18,'pbkdf2_sha256$100000$s1JRN2MoCsyv$Z24KXbXEIjwkPiUBo4BYiP+R9hxQcnUQwyF/Kq59xEE=',NULL,0,'mickel@dispostable.com','Mickel','Player two',0,1,'2019-02-28 19:40:27.000000','mickel@dispostable.com','image_user/filder.jpeg','player','12','batsman','mickho6f',4,5),(20,'pbkdf2_sha256$100000$DvbKZ7hWX80P$MIEIwlhIyu6ayFcLTP4M2IGj+hAj+vI1XJdLLrtPIo0=',NULL,0,'alpha@dispostable.com','Alpha','Player three',0,1,'2019-02-28 19:42:17.205137','alpha@dispostable.com','','player','12','bowler','alpho01o',4,5),(21,'pbkdf2_sha256$100000$vlXykk3Ziqn1$/BifnRlnvHYXTgjM1kiuPKMmeaoFsTuVs13/QSNJSlo=',NULL,0,'brain@dispostable.com','Brain','Player four',0,1,'2019-02-28 19:45:43.730305','brain@dispostable.com','','player','20','bowler','braidjpc',4,5),(22,'pbkdf2_sha256$100000$TtZ85ss9JJac$RC6I780LOzLuikLWRc9iq2i0QELzYHvQcUHJ6fffIeI=',NULL,0,'redmond@dispostable.com','Redmond','Player five',0,1,'2019-02-28 19:46:55.634423','redmond@dispostable.com','','player','25','wicket keeper','redm7vfc',4,5),(23,'pbkdf2_sha256$100000$cFcmz2hBb2mZ$m7E87r6W90ssnbT+gqbyEUwhylOAvmaRxztK/4gyO2Y=',NULL,0,'tamim@dispostable.com','tamim','Player one',0,1,'2019-02-28 19:49:27.781695','tamim@dispostable.com','','player','2','batsman','tami49v1',5,6),(24,'pbkdf2_sha256$100000$7QPEwSYWWq39$nlzpgPvFVjA0RIZKaQJe/AAL1clajCLtJhDyS/bvDfw=',NULL,0,'jahangir@dispostable.com','Jahangir','Player two',0,1,'2019-02-28 19:50:52.914364','jahangir@dispostable.com','','player','22','batsman','jahaym9q',5,6),(25,'pbkdf2_sha256$100000$5QxJqUDPt76v$P05BVdk9XSCsQHRABx1gDrvz8tC2+UP4lMHmrZMAehw=',NULL,0,'rafigul@dispostable.com','Rafigul','Player three',0,1,'2019-02-28 19:52:18.467494','rafigul@dispostable.com','','player','12','batsman','rafi3g37',5,6),(26,'pbkdf2_sha256$100000$NJ9R6tYzCBp5$7ImMblfqnXy55tZpzqX1fhM/E2bp6Jxzlt+l53lkmvI=',NULL,0,'azhar@dispostable.com','Azhar','Player four',0,1,'2019-02-28 19:53:09.863916','azhar@dispostable.com','','player','13','All rounder','azhat2zl',5,6);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_groups_user_id_group_id_40beef00_uniq` (`user_id`,`group_id`),
  KEY `user_groups_group_id_b76f8aba_fk_auth_group_id` (`group_id`),
  CONSTRAINT `user_groups_group_id_b76f8aba_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_groups_user_id_abaea130_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (1,1,1);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_user_permissions`
--

DROP TABLE IF EXISTS `user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_user_permissions_user_id_permission_id_7dc6e2e0_uniq` (`user_id`,`permission_id`),
  KEY `user_user_permission_permission_id_9deb68a3_fk_auth_perm` (`permission_id`),
  CONSTRAINT `user_user_permission_permission_id_9deb68a3_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_user_permissions_user_id_ed4a47ea_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_user_permissions`
--

LOCK TABLES `user_user_permissions` WRITE;
/*!40000 ALTER TABLE `user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-01  2:16:48
