from rest_framework import serializers
from match.models import Team, User

##player list Serializer
class PlayerListSerializer(serializers.Serializer):
    name = serializers.CharField(source = "playertype")
    id = serializers.CharField()


##Team listing Serializer
class TeamListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ('id','name')