from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from match.models import User, Country, Team, Match, ScoreCard, ScoreCardPlayer,MatchPlayerDetails
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.shortcuts import HttpResponseRedirect,redirect
from django.urls import reverse_lazy,reverse
from util.utility import generate_random_string,send_password_email,paginator_class
from django.db.models import Q,Max
from match.serializers import TeamListSerializer,PlayerListSerializer
from django.http import JsonResponse
# Create your views here.

## Login for user and player
class LoginView(View):

    def get(self,request):
        try:
            msg = request.session["msg"]
            del request.session["msg"]
        except:
            msg = ''
        return render(request,'common/login.html',{"msg": msg})

    def post(self,request):
        data = request.POST
        username = data.get('username')
        password = data.get('password')
        user = authenticate(username = username,password = password)

        if user:
            group_name = user.groups.filter(name = "admin")

        if user is not None and group_name.exists():
            login(request, user)
            if not data.get('options'):
                request.session.set_expiry(0)

            return redirect("match:dashboard")
        else:
            msg = "Username or Password is wrong"
            request.session["msg"] = msg
            return redirect("match:login")


## Logout for user and player
class LogoutView(View):

    def get(self,request):
        logout(request)
        return redirect("match:login")

class AdminProfileView(LoginRequiredMixin, View):

    def get(self, request):
        return render(request, "common/profile.html")

    def post(self, request):
        full_name = request.POST.get("name")
        if " " in full_name:
            first_name, last_name = full_name.split(" ", 1)
        else:
            first_name = full_name
            last_name = ''

        user = request.user
        user.image_uri = request.FILES.get("image", user.image_uri)
        user.email = request.POST.get("email", user.email)
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        return redirect("match:profile")

#### Functionility not working
class AdminChangePassword(LoginRequiredMixin, View):

    def get(self,request):
        try:
            msg = request.session["msg"]
            del request.session["msg"]
        except:
            msg = ''

        return render(request,"common/change_password.html",{"msg": msg})



class UserListView(LoginRequiredMixin, View):

    def get(self,request):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message =''

        query = request.GET.get('search')

        if query is not None:
            lookups = Q(first_name__icontains=query) | Q(username__icontains=query)| Q(email__icontains=query)|Q(last_name__icontains = query)
            user_list = User.objects.filter(lookups).exclude(groups__name='admin')
        else:
            user_list = User.objects.all().exclude(groups__name='admin')

        page = request.GET.get('page',1)
        records_per_page = 10
        user_list = paginator_class(user_list,page,records_per_page)

        context = {
            'users_list': user_list,
            "nbar": "user-lists",
            "message": message,
            "is_message": is_message
        }

        return render(request,'user_managements/usermanagement.html',context)


##Dashboard view
class DashboardView(LoginRequiredMixin,View):

    login_url='match/login/'

    def get(self,request):
        scorecard_obj = ScoreCard.objects.all()
        Max_runs_obj = scorecard_obj.order_by('-runs')
        Max_wicket_obj = scorecard_obj.order_by('-wicket')
        max_win_obj = ScoreCard.objects.filter(status="won").order_by('-status')
        max_win_count=max_win_obj.count()

        context={
            "Max_runs_obj":Max_runs_obj,
            "Max_wicket_obj":Max_wicket_obj,
            "max_win_obj":max_win_obj,
            "max_win_count":max_win_count
        }

        return render(request, "user_managements/dashboard.html",context)


## Player add functionaility
class UserAddView(LoginRequiredMixin,View):

    def get(self,request,*args, **kwargs):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message =''
        # user_group = self.request.user.groups.filter(name='admin').exists()
        # if user_group:

        countries = Country.objects.all()
        teams = Team.objects.all()
        context = {
            "title": "User Add",
            "url": reverse_lazy('match:user-add'),
            "countries": countries,
            "teams": teams,
            "message":message,
            "is_message":is_message
        }

        return render(self.request, 'user_managements/user_add.html', context)


    def post(self,request, *args, **kwargs):
        data = self.request.POST
        email = data.get("email")
        profile_img = self.request.FILES.get("image")
        first_name = data.get("first_name")
        last_name = data.get("last_name")
        jersy_number = data.get("jersy")
        country = Country.objects.get(id=data.get("country"))
        team = Team.objects.get(id=data.get("team"))
        player_type = data.get("player")
        password = email[:4]+generate_random_string(4)
        try:
            user = User.objects.create_user(username=email,password=password)
            send_password_email(email,password)
            user.email = email
            user.first_name = first_name
            user.last_name = last_name
            user.jersy_number = jersy_number
            user.country = country
            user.team = team
            user.player_type = player_type
            user.random_string = password
            user.save()
            message ="Added  successfully."
            is_message =1
            request.session["is_message"] = is_message
            request.session["message"] = message
        except:
            message = "Email already exist"
            is_message = 0
            request.session["is_message"] = is_message
            request.session["message"] = message
            return redirect("match:user-add")

        return redirect("match:user-lists")


##Country listing
class CountryListView(LoginRequiredMixin,View):

    def get(self,request,*args,**kwargs):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message = ''

        query = request.GET.get('search')

        if query is not None:
            lookups = Q(code__icontains=query) | Q(name__icontains=query)
            countries = Country.objects.filter(lookups)
        else:
            countries = Country.objects.all()

        page = request.GET.get('page', 1)
        records_per_page = 10
        countries = paginator_class(countries, page, records_per_page)

        context = {
            'countries': countries,
            "nbar": "countries",
            "message": message,
            "is_message": is_message
        }

        return render(request, 'countries/countries.html', context)


##Team listing
class TeamListView(LoginRequiredMixin,View):

    def get(self,request,*args,**kwargs):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message = ''

        query = request.GET.get('search')

        if query is not None:
            lookups = Q(code__icontains=query) | Q(name__icontains=query)
            teams = Team.objects.filter(lookups)
        else:
            teams = Team.objects.all()

        page = request.GET.get('page', 1)
        records_per_page = 10
        teams = paginator_class(teams, page, records_per_page)

        context = {
            'teams': teams,
            "nbar": "teams",
            "message": message,
            "is_message": is_message
        }

        return render(request, 'team/team.html', context)


##Match view logic
class MatchView(LoginRequiredMixin, View):

    def get(self,request):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message =''
        query = request.GET.get('search')

        if query is not None:
            lookups = Q(name__icontains=query) | Q(match_result__icontains=query)
            match_list = Match.objects.filter(lookups)
        else:
            match_list = Match.objects.all()

        page = request.GET.get('page',1)
        records_per_page = 10
        match_list = paginator_class(match_list, page, records_per_page)

        context = {
            'match_list': match_list,
            "nbar": "user-lists",
            "message": message,
            "is_message": is_message
        }

        return render(request,'match/matchview.html',context)



##Score view logic
class ScoreView(LoginRequiredMixin, View):

    def get(self,request):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message =''
        query = request.GET.get('search')

        if query is not None:
            lookups = Q(status__icontains=query) | Q(match__name__icontains=query) | Q(team__name__icontains=query) | Q(runs__icontains=query)| Q(wicket__icontains=query)| Q(point__icontains=query)
            score_list = ScoreCard.objects.filter(lookups)
        else:
            score_list = ScoreCard.objects.all()

        page = request.GET.get('page',1)
        records_per_page = 10
        score_list = paginator_class(score_list, page, records_per_page)

        context = {
            'score_list': score_list,
            "nbar": "score_list",
            "message": message,
            "is_message": is_message
        }

        return render(request,'score/score_view_list.html',context)


class ScoreAddView(LoginRequiredMixin, View):

    def get(self,request,*args, **kwargs):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message =''

        matchs = Match.objects.all()
        teams = Team.objects.all()
        status_list = [item[0] for item in ScoreCard.SCORE_STATUS_CHOICE]
        context = {
            "title": "Score Add",
            "url": reverse_lazy('match:score_add'),
            "matchs": matchs,
            "teams": teams,
            "status_list": status_list,
            "message":message,
            "is_message":is_message
        }
        print(context)
        return render(self.request, 'score/score_add.html', context)

    def post(self,request, *args, **kwargs):

        data = self.request.POST
        match = Match.objects.get(id=data.get("match"))
        team = Team.objects.get(id=data.get("team"))
        runs = data.get("runs")
        wicket = data.get("wicket")
        over = data.get("over")
        status = data.get("status")
        point = data.get("point")

        try:
            check_score = ScoreCard.objects.filter(match=match, team=team)
            if len(check_score) < 1:
                ScoreCard.objects.create(match=match, team=team, runs=runs, wicket=wicket, over=over, status=status, point=point)
                message ="Score added  successfully."
                is_message =1
                request.session["is_message"] = is_message
                request.session["message"] = message
            else:
                message ="Score already added for this match."
                is_message = 0
                request.session["is_message"] = is_message
                request.session["message"] = message
                return redirect("match:score_add")
        except:
            message = "Error in adding score"
            is_message = 0
            request.session["is_message"] = is_message
            request.session["message"] = message
            return redirect("match:score_add")

        return redirect("match:score_view")


##Match view logic
class MatchView(LoginRequiredMixin, View):

    def get(self, request):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message = ''

        query = request.GET.get('search')

        if query is not None:
            lookups = Q(name__icontains=query) | Q(match_result__icontains=query)
            match_list = Match.objects.filter(lookups)
        else:
            match_list = Match.objects.all()

        page = request.GET.get('page', 1)
        records_per_page = 10
        match_list = paginator_class(match_list, page, records_per_page)

        context = {
                'match_list': match_list,
                "nbar": "match_list",
                "message": message,
                "is_message": is_message
                }

        return render(request, 'match/matchview.html', context)



##Match addition logic
class MatchAddView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        try:
            message = request.session["message"]
            is_message = request.session["is_message"]
            del request.session["message"]
            del request.session["is_message"]
        except:
            message = ''
            is_message = ''

        teams = Team.objects.all()

        context = {
            "title": "Match Add",
            "url": reverse_lazy('match:match-add'),
            "teams": teams,
            "message": message,
            "is_message": is_message
            }
        return render(self.request, 'match/match_add.html', context)


    def post(self, request, *args, **kwargs):
        data = self.request.POST
        match = data.get("match")
        team1 = data.get("team1")
        team2 = data.get("team2")
        player1 = data.getlist("players1")
        player2 = data.getlist("players2")
        matchdate = data.get("matchdate")
        matchresult = data.get("matchresult")
        team1 = Team.objects.get(id=team1)
        team2 = Team.objects.get(id=team2)
        
        try:
            match_obj, created = Match.objects.get_or_create(name=match, team1=team1, team2=team2)
            match_obj.match_date = matchdate
            match_obj.match_result = matchresult
            match_obj.save()
            ### for First Team ####
            for player in player1:
                player_obj = User.objects.get(id=player)
                match_player_obj = MatchPlayerDetails.objects.create(match_id=match_obj, team_id=team1,player=player_obj)
             ## for Second Team ###
            for player in player2:
                player_obj = User.objects.get(id=player)
                match_player_obj = MatchPlayerDetails.objects.create(match_id=match_obj, team_id=team2, player=player_obj)

            message = "Added  successfully."
            is_message = 1
            request.session["is_message"] = is_message
            request.session["message"] = message
        except:
            message = "Same match record already added."
            is_message = 0
            request.session["is_message"] = is_message
            request.session["message"] = message
            return redirect("match:match-add")

        return redirect("match:match_view")


##Team listing Ajax
class AjaxTeamListView(LoginRequiredMixin, View):

    def post(self, request):
        team_id = request.POST.get('id')
        team_obj = Team.objects.all().exclude(id=team_id)
        team_list = TeamListSerializer(team_obj, many=True)
        team_obj = Team.objects.get(id=team_id)
        players_obj = team_obj.player.all()
        player_list = PlayerListSerializer(players_obj, many=True)

        if team_list is not None:
            result = team_list.data
            result1 = player_list.data
            status = 'success'
        else:
            result = ''
            status = 'error'

        return JsonResponse({'result': result, 'result1': result1, 'status': status})


##Player listing as per selected team in team creation
class AjaxPlayersListView(LoginRequiredMixin, View):

    def post(self, request):
        team_id = request.POST.get('id')
        team_obj = Team.objects.get(id=team_id)
        players_obj = team_obj.player.all()
        player_list = PlayerListSerializer(players_obj, many=True)

        if player_list is not None:
            result = player_list.data
            status = 'success'
        else:
            result = ''
            status = 'error'

        return JsonResponse({'result': result, 'status': status})


##Player view details logic
class PlayersView(LoginRequiredMixin, View):

    def get(self,request,*args, **kwargs):
        user_group = self.request.user.groups.filter(name='admin').exists()

        if user_group:
            user_obj = User.objects.get(pk=self.kwargs.get('pk'))
            context = {
                "user_obj":user_obj,
                }

        return render(self.request, 'team/player_detail.html', context)


##Player details after clicking o team
class PlayersListView(LoginRequiredMixin, View):

    def get(self,request,*args, **kwargs):
        user_group = self.request.user.groups.filter(name='admin').exists()

        if user_group:
            team_obj = Team.objects.get(pk=self.kwargs.get('pk'))
            query = request.GET.get('search')

            if query is not None:
                lookups = Q(first_name__icontains=query) | Q(last_name__icontains=query) | Q(player_type__icontains=query) | Q(
                    email__icontains=query)
                player_obj = team_obj.player.filter(lookups)
            else:
                player_obj = team_obj.player.all()

            page = request.GET.get('page', 1)
            records_per_page = 10
            player_obj = paginator_class(player_obj, page, records_per_page)

            context = {
                "player_obj":player_obj
                }

        return render(self.request, 'team/team_player_list.html', context)



##Team score card logic
class TeamScoreView(LoginRequiredMixin, View):

    def get(self,request,*args, **kwargs):
        user_group = self.request.user.groups.filter(name='admin').exists()

        if user_group:
            score_card_player_obj = ScoreCardPlayer.objects.filter(score_id=self.kwargs.get('pk'))
            query = request.GET.get('search')

            if query is not None:
                lookups = Q(player_id__first_name__icontains=query)
                score_card_player_obj = score_card_player_obj.filter(lookups)
            else:
                pass

            page = request.GET.get('page', 1)
            records_per_page = 10
            score_card_player_obj = paginator_class(score_card_player_obj, page, records_per_page)

            context = {
                "score_card_player_obj":score_card_player_obj,
                }

        return render(self.request, 'score/Team_score_detail.html', context)


##Functionlity not working
class ScorePlayerView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        return render(self.request, 'team/team_player_list.html')