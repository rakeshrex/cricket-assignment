from django.conf.urls import url
from . import views
# from .views import *
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

app_name = 'match'
urlpatterns = [
##Profile urls
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('profile/', views.AdminProfileView.as_view(), name='profile'),
    path('change_password/', views.AdminChangePassword.as_view(), name='change-password'),

##masters urls
    path('countries/', views.CountryListView.as_view(), name='country-lists'),
    path('user_list/', views.UserListView.as_view(), name='user-lists'),
    path('add/', views.UserAddView.as_view(), name='user-add'),
    path('teams/', views.TeamListView.as_view(), name='team-lists'),
    path('players/<int:pk>/', views.PlayersView.as_view(), name='player-view'),
    path('team/<int:pk>/', views.PlayersListView.as_view(), name='player-lists'),

##Dashboard urls
    path('dashboard/', views.DashboardView.as_view(), name='dashboard'),
    # path('chart/', views.PieChartView.as_view(), name='chart-data'),

##Match urls
    path('match/view/', views.MatchView.as_view(), name='match_view'),
    path('match/add/', views.MatchAddView.as_view(), name='match-add'),
    path('match/ajax_team_list/', views.AjaxTeamListView.as_view(), name='team-list'),
    path('match/ajax_players_list/', views.AjaxPlayersListView.as_view(), name='player-list'),


##Scorecard urls
    path('score_view/', views.ScoreView.as_view(), name='score_view'),
    path('score_add/', views.ScoreAddView.as_view(), name='score_add'),
    path('score_player_view/', views.ScorePlayerView.as_view(), name='score_player_view'),
    path('team/score/<int:pk>/', views.TeamScoreView.as_view(), name='team-score'),

]
