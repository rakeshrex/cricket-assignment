from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Country)
admin.site.register(DeviceDetail)
admin.site.register(Team)
admin.site.register(User)
admin.site.register(Match)
admin.site.register(MatchPlayerDetails)
admin.site.register(ScoreCard)
admin.site.register(ScoreCardPlayer)
