from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.db.models import Sum,Count


# Create your models here.

"""
Model class: Base Modal class
Depdency: none
"""
class BaseModal(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = "BaseModal"

"""
Model class: Country class
Depdency: Base model
"""
class Country(models.Model):
    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=80, blank=False, unique=True)
    flag = models.ImageField(upload_to='country_flag/', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "country"

"""
Model class: Team class
Depdency: Base model
"""
class Team(models.Model):
    name = models.CharField(max_length=80, unique=True)
    logo_uri = models.ImageField(upload_to='team_logo/', blank=False)
    club_state = models.CharField(max_length=80, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "team"

    @property
    def total_runs(self):
        runs_obj = ScoreCard.objects.filter(team=self.id)
        return runs_obj.aggregate(Sum('runs'))['runs__sum'] if runs_obj.exists() else 0
        # return total_runs

    @property
    def total_wickets(self):
        wickets_obj = ScoreCard.objects.filter(team=self.id)
        return wickets_obj.aggregate(Sum('wicket'))['wicket__sum'] if wickets_obj.exists() else 0

    @property
    def total_overs(self):
        overs_obj = ScoreCard.objects.filter(team=self.id)
        return overs_obj.aggregate(Sum('over'))['over__sum'] if overs_obj.exists() else 0

    @property
    def total_point(self):
        point_obj = ScoreCard.objects.filter(team=self.id)
        return point_obj.aggregate(Sum('point'))['point__sum'] if point_obj.exists() else 0

    @property
    def total_status(self):
        status_obj = ScoreCard.objects.filter(team=self.id)
        total_status = {}
        win=0; loss=0 ; draw=0
        for status in status_obj:
            if status.status == 'won':
                win=+1
            elif status.status == 'loss':
                loss=+1
            elif status.status == 'draw':
                draw=+1
            else:
                pass
        total_status["win"] = win ; total_status["loss"] = loss ; total_status["draw"] = draw
        return total_status

"""
Model class: User/Player abstracted class
Depdency: Base model, Country model
"""
class User(AbstractUser):

    USER_TYPE_CHOICE = (
        ('admin', 'Admin'),
        ('player', 'Player'),
    )

    PLAYER_TYPE_CHOICE = (
        ('batsman', 'Batsman'),
        ('bowler', 'Bowler'),
        ('wicket keeper', 'Wicket Keeper'),
        ('captain', 'Captain'),
        ('All rounder', 'All Rounder'),
    )

    email = models.EmailField(unique = True)
    image_uri = models.ImageField(upload_to='image_user/', blank=False)
    user_type = models.CharField(max_length=15, choices = USER_TYPE_CHOICE, default="player", blank=False)
    jersy_number = models.CharField(max_length=5, blank=False)
    country = models.ForeignKey(Country, null=True, blank=True, on_delete=models.CASCADE)
    team = models.ForeignKey(Team,related_name="player", null=True, blank=True, on_delete=models.CASCADE)
    player_type = models.CharField(max_length=20, choices = PLAYER_TYPE_CHOICE, default="all rounder", blank=False)
    random_string = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.email

    class Meta:
        db_table = "user"

    def playertype(self):
        return str(self.first_name)+" "+str(self.last_name)+" - "+str(self.player_type)

    @property
    def total_runs(self):
        runs_obj = ScoreCardPlayer.objects.filter(player_id=self.id)
        return runs_obj.aggregate(Sum('runs'))['runs__sum'] if runs_obj.exists() else 0

    @property
    def total_wickets(self):
        wickets_obj = ScoreCardPlayer.objects.filter(player_id=self.id)
        return wickets_obj.aggregate(Sum('wicket'))['wicket__sum'] if wickets_obj.exists() else 0

    @property
    def total_overs(self):
        overs_obj = ScoreCardPlayer.objects.filter(player_id=self.id)
        return overs_obj.aggregate(Sum('over'))['over__sum'] if overs_obj.exists() else 0

    @property
    def total_meadan_overs(self):
        meadan_overs_obj = ScoreCardPlayer.objects.filter(player_id=self.id)
        return meadan_overs_obj.aggregate(Sum('meadan_over'))['meadan_over__sum'] if meadan_overs_obj.exists() else 0

    @property
    def total_fours(self):
        fours_obj = ScoreCardPlayer.objects.filter(player_id=self.id)
        return fours_obj.aggregate(Sum('fours'))['fours__sum'] if fours_obj.exists() else 0

    @property
    def total_sixs(self):
        sixs_obj = ScoreCardPlayer.objects.filter(player_id=self.id)
        return sixs_obj.aggregate(Sum('sixs'))['sixs__sum'] if sixs_obj.exists() else 0

    @property
    def total_fifty(self):
        return ScoreCardPlayer.objects.filter(player_id=self.id).aggregate(Sum('fifty'))['fifty__sum']

    @property
    def total_hundred(self):
        return ScoreCardPlayer.objects.filter(player_id=self.id).aggregate(Sum('hundred'))['hundred__sum']

    @property
    def total_man_of_match(self):
        return ScoreCardPlayer.objects.filter(player_id=self.id,man_of_match=True).count


"""
Model class: Match details class
Depdency: Base model, Team model
"""
class DeviceDetail(BaseModal):
    PLATFORM_CHOICE = (
        ('android', 'android'),
        ('ios', 'ios'),
        ('unknown', 'unknown')
    )
    user = models.ForeignKey(User, related_name= "user_device_detail", on_delete= models.CASCADE)
    platform = models.CharField(max_length= 30, choices= PLATFORM_CHOICE, default= "unknown")
    device_id = models.CharField(max_length= 200, null= False, blank= False, unique= True)
    device_token = models.TextField(null= True, blank= True)
    app_version = models.CharField(max_length= 100, null= True, blank= True)
    access_token = models.TextField(null= True, blank= True)
    refresh_token = models.TextField(null= True, blank= True)

    def __str__(self):
        return self.platform

    def is_ios(self):
        return True if self.platform in ["ios"] else False

    def is_android(self):
        return True if self.platform in ["android"] else False

    class Meta:
        db_table = "device_detail"



"""
Model class: Match details class
Depdency: Base model, Team model
"""
class Match(models.Model):
    name = models.CharField(max_length = 100, blank=False)
    team1 = models.ForeignKey(Team, related_name="team1", blank=False, on_delete=models.CASCADE)
    team2 = models.ForeignKey(Team, related_name="team2", blank=False, on_delete=models.CASCADE)
    match_date = models.DateTimeField(null=True, blank=True)
    match_result = models.CharField(max_length=100, null=True, blank= True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "match"


"""
Model class: Match Player details class
Depdency: Base model, User model, Team modal
"""
class MatchPlayerDetails(BaseModal):
    match_id = models.ForeignKey(Match, related_name="match_player", blank=False, on_delete=models.CASCADE)
    team_id = models.ForeignKey(Team, related_name="match_team", blank=False, on_delete=models.CASCADE)
    player = models.ForeignKey(User, related_name="user_player", blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return self.match_id.name

    class Meta:
        db_table = "match_player_detail"


"""
Model class: ScoreCard class
Depdency: Base model, User model, Team modal
"""
class ScoreCard(BaseModal):
    SCORE_STATUS_CHOICE = (
        ('won', 'Won'),
        ('loss', 'Loss'),
        ('draw', 'Draw'),
    )

    match = models.ForeignKey(Match, related_name="score_card_match", blank=False, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, related_name="score_card_team", blank=False, on_delete=models.CASCADE)
    runs = models.IntegerField(blank=False, default=0)
    wicket = models.IntegerField(blank=False, default=0)
    over = models.FloatField(blank=False, default=0.0)
    status = models.CharField(max_length=10, choices = SCORE_STATUS_CHOICE, blank=True)
    point = models.IntegerField(blank=False, default=0)

    def __str__(self):
        return self.match.name

    class Meta:
        db_table = "score_card"

    def score_status_list(self):
        return self.status


"""
Model class: ScoreCardPlayer class
Depdency: Base model, User model, Team modal
"""
class ScoreCardPlayer(BaseModal):
    PLAYER_STATUS_CHOICE = (
        ('out', 'Out'),
        ('notout', 'Not Out'),
        ('notplayed', 'Not Played'),
    )

    score = models.ForeignKey(ScoreCard, related_name="score_player", blank=False, on_delete=models.CASCADE)
    player_id = models.ForeignKey(User, related_name="player_score", blank=False, on_delete=models.CASCADE)
    runs = models.IntegerField(blank=False, default=0)
    wicket = models.IntegerField(blank=False, default=0)
    over = models.FloatField(blank=False, default=0.0)
    fifty = models.IntegerField(blank=False, default=0)
    hundred = models.IntegerField(blank=False, default=0)
    meadan_over = models.FloatField(blank=False, default=0)
    fours = models.IntegerField(blank=False, default=0)
    sixs = models.IntegerField(blank=False, default=0)
    man_of_match = models.BooleanField(blank=False, default=False)
    status = models.CharField(max_length=10, choices = PLAYER_STATUS_CHOICE, blank=True)

    def __str__(self):
        return self.player_id.first_name

    class Meta:
        db_table = "score_card_player"
