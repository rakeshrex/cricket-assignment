from django.urls import path
from api import views

app_name = 'api'

urlpatterns = [
    ## User login details URL
    path('signup/',views.UserSignupAPIView.as_view(),name='signup'),
    path('login/',views.UserLoginAPIView.as_view(),name='login'),

    path('genrics/country_list/', views.CountryListAPIView.as_view(), name= 'country_list'),
    path('view/country_add/', views.CountryAddAPIView.as_view(), name= 'country_add'),

]


