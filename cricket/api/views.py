from rest_framework import generics, status, mixins
from django.contrib.auth import get_user_model, authenticate
from rest_framework.permissions import (AllowAny, IsAuthenticated )
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserCreateSerializer, UserLoginSerializer, GetImageSerializer, CountrySerializer, CountryAddSerializer, CountryUpdateSerializer, CountryDeleteSerializer
from match.models import User, DeviceDetail, Country
from util.utility import (validate_headers, error_response, success_response)
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer

## Signup API
class UserSignupAPIView(APIView):
    serializer_class = UserCreateSerializer
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserCreateSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            platform = request.META.get('HTTP_PLATFORM', None)
            app_version = request.META.get('HTTP_APP_VERSION', None)
            device_token = request.META.get('HTTP_DEVICE_TOKEN', None)
            device_id = request.META.get('HTTP_DEVICE_ID', None)
            user_type = request.META.get('HTTP_USER_TYPE',None)

            validate_headers(platform, device_id, app_version)

            return_data = {}
            username = data['username']
            password = data['password']
            user_type = data['user_type']
            full_name = data['full_name']

            user_obj = User.objects.filter(username=username)

            if user_obj.exists():
                message = "User already exists"
                return error_response(message, return_data )

            try:
                user_obj = User.objects.create_user(username = username, password = password, email=username, user_type= user_type, first_name= full_name)
            except:
                message = "Request parameter is not formated."
                return error_response(message, return_data)

            message = "Signup success."
            user_obj.save()
            return success_response(message,return_data)


## Login API
class UserLoginAPIView(APIView):
    serializer_class = UserLoginSerializer
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            platform = request.META.get('HTTP_PLATFORM', None)
            device_token = request.META.get('HTTP_DEVICE_TOKEN', None)
            app_version = request.META.get('HTTP_APP_VERSION', None)
            device_id = request.META.get('HTTP_DEVICE_ID', None)
            user_type = request.META.get('HTTP_USER_TYPE', None)

            validate_headers(platform,device_id,app_version)
            return_data = {}

            username = data['username']
            password = data['password']
            user_obj = User.objects.filter(username= username)

            if not user_obj.exists():
                message = "User is not registered with us."
                return error_response(message, return_data )

            if user_obj[0].is_active == False:
                message = "Inactive user"
                return error_response(message, return_data )

            user_obj = authenticate(username= username, password= password)

            if not user_obj:
                message = "Invalid Credentials"
                return error_response(message, return_data)

            serializer = GetImageSerializer(user_obj, context={'request': request})

            obj = TokenObtainPairSerializer()
            token = obj.validate({"username": username, "password": password})

## One user can login only one device. One user can login multiple devices
            try:
                device_detail, created = DeviceDetail.objects.update_or_create(device_id= device_id, user = user_obj)
                device_detail.user = user_obj
                device_detail.platform = platform
                device_detail.device_token = device_token
                device_detail.app_version = app_version
                device_detail.access_token = token.get('access')
                device_detail.refresh_token = token.get('refresh')
                device_detail.save()
            except:
                message = "User already login with same device."
                return error_response(message, return_data)

            return_data ={ "user_id": user_obj.id, "username": user_obj.username, "name": user_obj.first_name, "profile_image":serializer.data['image_uri'],
                   "access_token": token.get('access'), "refresh_token": token.get('refresh')}

        message = "success"
        return success_response(message, return_data)


## Using generic view
class CountryListAPIView(generics.ListAPIView ):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return self.list(request)

## Using API View
class CountryAddAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        serializer = CountryAddSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            platform = request.META.get('HTTP_PLATFORM', None)
            app_version = request.META.get('HTTP_APP_VERSION', None)
            device_token = request.META.get('HTTP_DEVICE_TOKEN', None)
            device_id = request.META.get('HTTP_DEVICE_ID', None)

            validate_headers(platform, device_id, app_version)

            return_data = {}
            name = data['name']
            code = data['code']
            flag = request.FILES['flag']

            country_obj = Country.objects.filter(code=code, name=name)

            if country_obj.exists():
                message = "Country and Code already exists"
                return error_response(message, return_data )

            try:
                country_obj = Country.objects.create(code = code, name = name, flag = flag)
                country_obj.save()
            except:
                message = "Request parameter is not formated."
                return error_response(message, return_data)

            message = "success."
            return success_response(message,return_data)


    def put(self, request):
        data = request.data
        serializer = CountryUpdateSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            platform = request.META.get('HTTP_PLATFORM', None)
            app_version = request.META.get('HTTP_APP_VERSION', None)
            device_token = request.META.get('HTTP_DEVICE_TOKEN', None)
            device_id = request.META.get('HTTP_DEVICE_ID', None)

            validate_headers(platform, device_id, app_version)
            return_data = {}

            id = data['id']
            name = data['name']
            code = data['code']
            flag = request.FILES['flag']

            try:
                country_obj = Country.objects.get(id=id)
            except:
                message = "Invalid or wrong country ID"
                return error_response(message, return_data)

            if name:
                country_obj.name = name
            elif code:
                country_obj.code = code
            elif flag:
                country_obj.flag = flag

            country_obj.save()

            message = "success."
            return success_response(message, return_data)

    def delete(self, request):
        data = request.data
        serializer = CountryDeleteSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            platform = request.META.get('HTTP_PLATFORM', None)
            app_version = request.META.get('HTTP_APP_VERSION', None)
            device_token = request.META.get('HTTP_DEVICE_TOKEN', None)
            device_id = request.META.get('HTTP_DEVICE_ID', None)

            validate_headers(platform, device_id, app_version)
            return_data = {}

            id = data['id']
            try:
                country_obj = Country.objects.get(id=id)
            except:
                message = "Invalid or wrong country ID"
                return error_response(message, return_data)

            country_obj.delete()

            message = "success."
            return success_response(message, return_data)
