from rest_framework.permissions import BasePermission

class IsAllowed(BasePermission):


    def has_permission(self, request, view):
        import pdb
        pdb.set_trace()

        return request.user.groups.values_list('name',flat =True)[0]==request.META.get("HTTP_USER_TYPE")