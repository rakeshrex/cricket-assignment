from django.db.models import Avg
from rest_framework import serializers
from match.models import Country


class UserCreateSerializer(serializers.Serializer):
    user_type = serializers.CharField(required=True)
    full_name = serializers.CharField(required=True)
    username = serializers.CharField(min_length=10, max_length=12, required=True)
    password = serializers.CharField(min_length=8, write_only=True, required=True)



class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, min_length=8, write_only=True)


class GetImageSerializer(serializers.Serializer):
    image_uri = serializers.SerializerMethodField()

    def get_image_uri(self, obj):
        if obj.image_uri:
            request = self.context.get('request')
            image_uri = obj.image_uri.url
            return request.build_absolute_uri(image_uri)
        else:
            image_uri = ""
            return image_uri

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['id','code','name', 'flag']

class CountryAddSerializer(serializers.Serializer):
    code = serializers.CharField(required=True, min_length=3)
    name = serializers.CharField(required=True)
    flag = serializers.ImageField(required=True)

class CountryUpdateSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    code = serializers.CharField(required=False)
    name = serializers.CharField(required=False)
    flag = serializers.ImageField(required=False)

class CountryDeleteSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)